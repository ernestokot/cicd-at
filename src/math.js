function add(a, b) {
  return a + b;
}

function absoluteAdd(a, b) {
  if (a < 0 && b > 0) {
    return b - a;
  }
  
  if (b < 0 && a > 0) {
    return a - b;
  }
  
  if (b < 0 && a < 0) {
    return -1 * (a + b);
  }

  return a + b;
}

function squareRootSum(a, b){
  return (a * a ) + (b * b)
}

function absoluteDifference(a, b){
  if( a < 0 ) {
    a = a * -1;
  }

  if( b < 0 ) {
    b = b * -1;
  }

  if( a > b ){
    return a - b;
  } 

  return b - a;
}

module.exports = { add, absoluteAdd,  squareRootSum, absoluteDifference};

