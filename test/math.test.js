const { expect } = require("chai");

const { multiply } = require("../src/product");
const { add, absoluteAdd, squareRootSum, absoluteDifference } = require("../src/math");


describe("Math", function() {
  describe("multiply", function() {
    it("multiplys two numbers together", function() {
      expect(multiply(1, 2)).to.eq(2);
    });
  });

  describe("add", function() {
    it("adds two numbers together", function() {
      expect(add(1, 2)).to.eq(3);
    });
  });

  describe("absoluteAdd", function() {
    context("when the numbers are both positive", function() {
      it("returns the correct value", function() {
        expect(absoluteAdd(1, 2)).to.eq(3);
      });
    });

    context("when the numbers are both negative", function() {
      it("returns the correct value", function() {
        expect(absoluteAdd(-1, -2)).to.eq(3);
      });
    });

    context("when the first is negative and the last is positive", function() {
      it("returns the correct value", function() {
        expect(absoluteAdd(-1, 2)).to.eq(3);
      });
    });

    context("when the first is positive and the last is negative", function() {
      it("returns the correct value", function() {
        expect(absoluteAdd(1, -2)).to.eq(3);
      });
    });

    context("sum of square root of two numbers", function() {
      it("returns the correct value", function() {
        expect(squareRootSum(2, 3)).to.eq(13);
      });
    });

  });

  describe("absoluteDifference", function() {
    context("when both are negative", function() {
      it("returns the correct value", function() {
        expect(absoluteDifference(-1, -2)).to.eq(1);
      });
    });

    context("when one is negative and other positive", function() {
      it("returns the correct value", function() {
        expect(absoluteDifference(1, -2)).to.eq(1);
      });
    });

    context("when both are positive", function() {
      it("returns the correct value", function() {
        expect(absoluteDifference(1, 2)).to.eq(1);
      });
    });

    context("when first is absolutely greater than second", function() {
      it("returns the correct value", function() {
        expect(absoluteDifference(3, 2)).to.eq(1);
      });
    });

  });

});
