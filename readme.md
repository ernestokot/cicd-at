# Test CI/CD Project

Cool Saturday morning learning CI/CD at Africa's Talking offices. Facilitate by Ernest Okot (okot08@gmail.com)

Attendees;
- Mark Okello (markokello55@gmail.com)
- Bertha Kembabazi (kembertha348@gmail.com)
- Hillary Mumbere (hmumbere@gmail.com)
- Grace Kyeyune (gracekyeyune@gmail.com)
- David Okwii (oquidave@gmail.com)
- Edison Abahurire (abahedison@gmail.com)
- Mark Phillip (markphi119@gmail.com)
- Zerga Kyuk (zergakyuk@gmail.com) 

Join [slack](https://join.slack.com/t/freeformco/shared_invite/enQtNjAwMDUyNjgyMzM2LTk0MjZiOThkMTllZDFmNTQ4NmM3MjljYTNkODNhOTBhODBjMjM0YjdiMWY3MGJiN2I2MDg0NDM5ZTM5NmRkOGE) to continue the conversation